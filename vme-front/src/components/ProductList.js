import React, { useState, useEffect } from 'react';

const ProductList = ({c}) => {
    const [products, setProducts] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalProducts, setTotalProducts] = useState(0);
    const [productsPerPage, setProductsPerPage] = useState(10);
    const [cart, setCart] = c;

    useEffect(() => {
        fetch(`http://api.vme.local/products?page=${currentPage}`)
            .then(response => response.json())
            .then((data) => {
                setProducts(data.data)
                setProductsPerPage(data.meta.count)
                setTotalProducts(data.meta.total)
            })
            .catch(error => console.error(error));
    }, [currentPage]);

    const addToCart = (product) => {
        const existingProductIndex = cart.findIndex((p) => p.id === product.id);

        if (existingProductIndex !== -1) {
            const updatedCart = [...cart];
            updatedCart[existingProductIndex].quantity += 1;
            setCart(updatedCart);
        } else {
            setCart([...cart, { ...product, quantity: 1 }]);
        }
    };


    const removeFromCart = (product) => {
        setCart(cart.filter(p => p.id !== product.id));
    };

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return (
        <div>
            <h3>Product list</h3>
            <table>
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Brand</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {products.map(product => (
                    <tr key={product.id}>
                        <td><img style={{height:50}} src={product.image_url} alt={product.name} /></td>
                        <td>{product.name}</td>
                        <td>{product.brand}</td>
                        <td>{product.price}</td>
                        <td>
                            <button onClick={() => addToCart(product)}>Add to cart</button>
                            {cart.some(p => p.id === product.id) && (
                                <button onClick={() => removeFromCart(product)}>Remove from cart</button>
                            )}
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
            <Pagination
                productsPerPage={productsPerPage}
                totalProducts={totalProducts}
                paginate={paginate}
                currentPage={currentPage}
            />
        </div>
    );
};

const Pagination = ({ productsPerPage, totalProducts, paginate, currentPage }) => {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalProducts / productsPerPage); i++) {
        pageNumbers.push(i);
    }

    const renderPageNumbers = () => {
        const pageDiff = 2; // Show 2 pages before and after current page
        const filteredPageNumbers = pageNumbers.filter(
            pageNumber => pageNumber === 1 || pageNumber === currentPage || pageNumber === pageNumbers.length || (pageNumber >= currentPage - pageDiff && pageNumber <= currentPage + pageDiff)
        );

        return filteredPageNumbers.map(number => (
            <button key={number} onClick={() => paginate(number)}>
                {number}
            </button>
        ));
    };

    return (
        <div>
            {renderPageNumbers()}
        </div>
    );
};

export default ProductList;
