import React from 'react';

const Cart = ({ cart, setCart, sendOrder }) => {
    const removeFromCart = (product) => {
        setCart(cart.filter(p => p.id !== product.id));
    };

    if(!cart.length) {
        return (<></>)
    }

    let sum = 0;
    for(let i = 0; i < cart.length; i++) {
        sum += parseFloat(cart[i].price.toFixed(2));
    }

    sum = parseFloat(sum.toFixed(2));

    return (
        <div>
            <h3>Cart content</h3>
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {cart.map((product) => (
                    <tr key={product.id}>
                        <td>{product.name}</td>
                        <td><input className="quantity-input" value={product.quantity} /></td>
                        <td>€{product.price}</td>
                        <td><button onClick={() => removeFromCart(product)}>Remove</button></td>
                    </tr>
                ))}
                <tr>
                    <td colSpan={2} style={{textAlign:"right"}}>Sum:</td>
                    <td><strong>€{sum}</strong></td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <button className="order-send" onClick={sendOrder}>Send order</button>
        </div>
    )
};

export default Cart;
