import './App.css';
import ProductList from "./components/ProductList";
import Cart from "./components/Cart";
import {useState} from "react";

function App() {
    const [cart, setCart] = useState([]);

    const sendOrder = () => {
        const order = {
            user_id: 2,
            products: []
        }

        cart.map((v) => {
            order.products.push({
                product_id: v.id,
                quantity: 1
            })
        })

        fetch('http://api.vme.local/orders', {
            method: 'POST',
            body: JSON.stringify(order)
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                alert('Order sent');
                setCart([]);
                console.log(data); // Do something with the response data
            })
            .catch(error => {
                console.error('There was a problem with the fetch operation:', error);
            });
        console.log('send', cart);
    }

    return (
        <div className="App">
            <div className="product-list-container">
                <Cart cart={cart} setCart={setCart} sendOrder={sendOrder}/>
            </div>

            <div className="product-list-container">
                <ProductList c={[cart, setCart]}/>
            </div>
        </div>
    );
}

export default App;
