# Installation

Add to `/etc/hosts`
```shell
0.0.0.0 api.vme.local vme.local
```

In bash: 
```shell
./install.sh
```

Open http://vme.local/ in your browser.

# Technical Test

The goal of this test is for you to showcase your abilities by building a small small eCommerce shop. You may implement all of the below or parts of it. In either case, please time yourself so we will have some context when discussing your approach and decision making in the upcoming interview.

## Solution

It’s composed of 2 parts:

-   The backend: handling the HTTP routing, exposing a defined list of API endpoints
-   The frontend: an optional UI for the API, built in the tool you have the most experience in (Flutter, JS framework, etc).

The initial import dataset can be found attached to this gist.

## Backend

Build the **smallest** MVC framework able to route an HTTP query and send the response. The framework does not need to handle more than what is required for the app.

Note: _build_ means using the least amount of third-party packages possible. We do recommend using an ORM like [](https://github.com/illuminate/database)[https://github.com/illuminate/database](https://github.com/illuminate/database) to handle database queries/connections, but apart from that, we expect the rest of the bits of the framework to be done by the candidate.

The app needs to implement the following endpoints:

-   GET /products => returns a paginated list of products that can be sorted by name, price (both asc and desc)
-   POST /orders => Create a new order, and return the order details
-   GET /orders/:id => Retrieve the details of the given order
-   POST /orders/:id/items => adds a new item to the given order
-   DELETE /orders/:id/items/:item_id => removes the item from the given order
-   POST /orders/:id/payment (optional) => This method integrates with Stripe backend and will receive a payment method id (you can create one in stripe manually to test this) and will try to attempt to authorize and capture the amount total of the order.

## Front End

If you decide to build this, we recommend it has the following features:

-   Browse products
-   Add products to the cart
-   Show cart
-   Checkout (pay) - no need to integrate to stripe from the client side - you can send always the same hardcoded payment method ID.

## Evaluation Criteria

Please push your code to a (public or private) GitHub repository and deploy it somewhere so we can test it. We’ll evaluate:

-   the architecture of the framework,
-   the quality of the code,
-   and the UI & UX of the search (if provided).

Good luck!
