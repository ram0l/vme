#!/usr/bin/env bash

docker-compose -f docker-compose.yml up -d && \
docker exec -it vme_php php composer install && \
docker run -v "$PWD"/vme-front:/usr/src/app -w /usr/src/app node:16.18.1 sh -c "npm install" && \
docker run -v "$PWD"/vme-front:/usr/src/app -w /usr/src/app node:16.18.1 sh -c "npm run build"

