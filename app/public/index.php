<?php

// Define root directory
define('rootDir', __DIR__.'/..');
include rootDir.'/vendor/autoload.php';
$__config = include rootDir.'/config/config.php';
function dd($x, $data)
{
    echo '<pre><b>'.$x."</b>\n";
    print_r($data);
    echo '</pre>';
}

use Illuminate\Database\Capsule\Manager as Capsule;
use Vme\View\ViewInterface;

$capsule = new Capsule;
$capsule->addConnection($__config['database']);

// Set the event dispatcher used by Eloquent models (optional)
$capsule->setEventDispatcher(new Illuminate\Events\Dispatcher(new Illuminate\Container\Container));
$capsule->setAsGlobal();
$capsule->bootEloquent();

// Register autoloader
spl_autoload_register(function ($class) {
    // PSR-4 autoloading
    $prefix = 'App\\';
    $baseDir = rootDir.'/src/';
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // Not in Vme namespace, skip
        return;
    }

    $relativeClass = substr($class, $len);
    $file = $baseDir.str_replace('\\', '/', $relativeClass).'.php';
    if (file_exists($file)) {
        require $file;
    }
});

spl_autoload_register(function ($class) {
    // PSR-4 autoloading
    $prefix = 'Vme\\';
    $baseDir = rootDir.'/framework/';
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // Not in Vme namespace, skip
        return;
    }
    $relativeClass = substr($class, $len);
    $file = $baseDir.str_replace('\\', '/', $relativeClass).'.php';
    if (file_exists($file)) {
        require $file;
    }
});


// Set up error handler
set_exception_handler(function (\Error $exception) {
    $controller = new \App\Controllers\ErrorController();
    $controller->json([
        'error' => $exception->getMessage(),
        'file' => $exception->getFile(),
        'trace' => $exception->getTrace(),
    ]);
});

// Get requested URL
$requestUrl = $_SERVER['REQUEST_URI'] ?? '/';
$method = strtolower($_SERVER['REQUEST_METHOD']) ?? 'get';
$requestUrl = strtok($requestUrl, '?'); // Remove query string

// Define routes
$router = new \Vme\Router\Router();
$router->addRoute('get', '/products', \App\Controllers\ProductsController::class, 'index');
$router->addRoute('post', '/orders', \App\Controllers\OrdersController::class, 'store');
$router->addRoute('get', '/orders/:id', \App\Controllers\OrdersController::class, 'show', ['id' => '[0-9]+']);
$router->addRoute('post', '/orders/:id/items', \App\Controllers\OrdersController::class, 'storeItem', ['id' => '[0-9]+']);
$router->addRoute('delete', '/orders/:id/items/:item_id', \App\Controllers\OrdersController::class, 'delete', ['id' => '[0-9]+', 'item_id' => '[0-9]+']);
$router->addRoute('post', '/orders/:id/payment', \App\Controllers\OrdersController::class, 'payment', ['id' => '[0-9]+']);

// Check if route exists
$routes = $router->getRoutes();
$match = $router->match($method, $requestUrl);

// Check if route exists
if (!empty($match)) {
    // Get controller name from route
    $controllerName = $match['controller'];
    $actionName = $match['action'];

    // Use class from the namespace
    $controller = new $controllerName();

    $request = new \Vme\Request\Request($match['params']);
    $response = new Vme\Response\JsonResponse();

    // Call appropriate method
    if (method_exists($controller, $actionName)) {
        try {
            /** @var \Vme\Response\ResponseInterface $view */
            $response = $controller->{$actionName}($request, $response);
            $response->sendHeaders();
            echo $response->send();
        } catch (Exception $e) {
            $response->setCode(500);
            $response->sendHeaders();
            $response->setBody(['error' => $e->getMessage()]);
            echo $response->send();
        }
    }
} else {
    $response = new Vme\Response\JsonResponse();
    $response->setCode(404);
    $response->sendHeaders();
    $response->setBody(['error' => 'Not found']);
    echo $response->send();
}


