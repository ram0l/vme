<?php

namespace Vme\Router;

class Router
{
    private array $routes = [];

    /**
     * Add route to the list
     *
     * @param string $method
     * @param string $path
     * @param string $controller
     * @param string $action
     * @param array $params
     * @return $this
     */
    public function addRoute(string $method, string $path, string $controller, string $action, array $params = []): self
    {
        $this->routes[$method][$path] = [$controller, $action, $params];

        return $this;
    }

    /**
     * Get routes
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param string $method
     * @param string $requestUrl
     * @return array
     */
    public function match(string $method, string $requestUrl): array
    {
        foreach ($this->routes[$method] as $route => $data) {
            $params = [];
            foreach ($data[2] as $param => $value) {
                preg_match('#:'.$param.'#', $route, $matches);
                if (isset($matches[0])) {
                    $params[trim($param)] = null;
                }
                $route = str_replace(':'.$param, '('.$value.')', $route);
            }
            if (preg_match('#^'.$route.'$#', $requestUrl, $matches)) {
                $idx = 1;
                foreach ($params as $param => $value) {
                    $params[trim($param)] = $matches[$idx++];
                }

                return [
                    'controller' => $data[0],
                    'action' => $data[1],
                    'params' => $params
                ];
            }
        }


        return [];
    }
}
