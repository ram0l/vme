<?php

namespace Vme\Response;

class JsonResponse extends Response
{
    public function sendHeaders(): void
    {
        parent::sendHeaders();
        header('Content-Type: application/json');
    }


    public function send(): string
    {
        return \json_encode($this->body);
    }
}
