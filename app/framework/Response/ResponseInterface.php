<?php

namespace Vme\Response;

interface ResponseInterface
{
    public function sendHeaders(): void;
    public function send(): string;
}
