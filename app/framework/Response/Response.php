<?php

namespace Vme\Response;

abstract class Response implements ResponseInterface
{
    /** @var int HTTP Code */
    protected int $code = 200;

    /** @var mixed */
    protected mixed $body;

    public function sendHeaders(): void
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

        switch ($this->code) {
            case 200:
                header("HTTP/1.0 200 OK");
                break;
            case 404:
                header("HTTP/1.0 404 Not Found");
                break;
            case 500:
                header("HTTP/1.0 500 Internal Server Error");
                break;
            default:
                header("HTTP/1.0 400 Bad Request");
        }
    }

    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @param mixed $data
     * @return void
     */
    public function setBody(mixed $data): void
    {
        $this->body = $data;
    }
}
