<?php

namespace Vme\View;

class JsonView implements ViewInterface
{
    protected array $data = [];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return json_encode($this->data);
    }
}
