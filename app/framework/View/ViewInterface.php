<?php

namespace Vme\View;

interface ViewInterface
{
    public function render(): string;
}
