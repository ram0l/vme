<?php

namespace Vme\Request;

class Request
{
    /** @var string|mixed */
    private string $method = 'get';

    /** @var array */
    private array $params = [];

    /** @var array */
    private array $query = [];

    /** @var array */
    private array $jsonBody = [];

    /**
     * Constructor
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->query = $_REQUEST;
        $this->jsonBody = $this->getJsonBody();
        $this->params = $params;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function input(string $key): mixed
    {
        return $this->jsonBody[$key] ?? null;
    }

    /**
     * @param string $name
     * @param string|null $default
     * @return string|null
     */
    public function query(string $name, string $default = null): ?string
    {
        return $this->query[$name] ?? $default;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param string $name
     * @param string|null $default
     * @return string|null
     */
    public function getParam(string $name, string $default = null): ?string
    {
        return $this->params[$name] ?? $default;
    }

    /**
     * @return array|null
     */
    private function getJsonBody(): ?array
    {
        if ($this->jsonBody === []) {
            $jsonString = file_get_contents('php://input');
            if ($jsonString === "") {
                return [];
            }
            $this->jsonBody = json_decode($jsonString, true);
        }

        return $this->jsonBody;
    }
}
