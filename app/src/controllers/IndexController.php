<?php

namespace App\Controllers;

use App\Models\Products;

class IndexController extends BaseController
{
    public function index()
    {
        $model = Products::find(1);

        echo json_encode($model->toArray());
    }
}
