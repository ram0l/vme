<?php

namespace App\Controllers;

use App\Models\Products;
use Vme\Request\Request;
use Vme\Response\Response;
use Vme\Response\ResponseInterface;
use Vme\View\JsonView;
use Vme\View\ViewInterface;

class ProductsController extends BaseController
{
    /**
     * Returns a paginated list of products that can be sorted by name, price (both asc and desc)
     *
     * @param Request $request
     * @param Response $response
     * @return ResponseInterface
     */
    public function index(Request $request, Response $response): ResponseInterface
    {
        $orderBy = $request->query('orderBy', 'id');
        $sortBy = $request->query('sortBy', 'asc');
        $page = $request->query('page', 1);
        $limit = 10;
        $page--;

        $products = Products::query()
            ->orderBy($orderBy, $sortBy)
            ->offset($page * $limit)
            ->limit($limit)
            ->get()
            ->toArray();

        $count = Products::count();

        $response->setBody([
            'data' => $products,
            'meta' => [
                'total' => $count,
                'count' => $limit,
                'page' => $page + 1,
                'total_pages' => ceil($count / $limit),
            ],
        ]);

        return $response;
    }
}
