<?php

namespace App\Controllers;

use App\Models\Orders;
use App\Models\OrdersItems;
use App\Models\Products;
use Stripe\Exception\ApiErrorException;
use Vme\Request\Request;
use Vme\Response\Response;
use Vme\Response\ResponseInterface;

class OrdersController extends BaseController
{
    /**
     * Create a new order, and return the order details
     *
     * @param Request $request
     * @param Response $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function store(Request $request, Response $response): ResponseInterface
    {
        if(!$request->input('user_id')) {
            throw new \Exception('Missing user_id');
        }
        if(!$request->input('products')) {
            throw new \Exception('Missing products');
        }

        try {
            $productIds = [];
            $sum = 0;
            foreach ($request->input('products') as $product) {
                $productIds[$product['product_id']] = $product['quantity'];
            }

            Products::find(array_keys($productIds))->each(function ($product) use (&$sum, $productIds) {
                $sum += $product->price * $productIds[$product->id];
            });

            $model = new Orders();
            $model->status = 'new';
            $model->sum = round($sum,2);
            $model->save();

            $products = [];
            foreach ($request->input('products') as $product) {
                if(!isset($product['quantity'])) {
                    $product['quantity'] = 1;
                }
                if(!isset($product['product_id'])) {
                    throw new \Exception('Missing product_id');
                }

                $products[] = [
                    'order_id' => $model->id,
                    'product_id' => $product['product_id'],
                    'quantity' => $product['quantity'],
                ];
            }

            OrdersItems::insert($products);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            throw $e;
        }

        $response->setBody($model->load(['products', 'products.product'])->toArray());

        return $response;
    }

    /**
     * Stores an item in an order
     *
     * @param Request $request
     * @param Response $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function storeItem(Request $request, Response $response): ResponseInterface
    {
        $orderId = (int)$request->getParam('id');
        $order = Orders::with('items')->find($orderId);

        if (!$order) {
            $response->setCode(404);
            $response->setBody(['status' => 'Not Found']);

            return $response;
        }

        try {
            $item = new OrdersItems();
            $item->order_id = $orderId;
            $item->product_id = $request->input('product_id');
            $item->quantity = $request->input('quantity');
            $item->save();
        } catch (\Exception $e) {
            error_log($e->getMessage());
            throw $e;
        }

        $response->setBody([
            'status' => 'success',
            'data' => $item->toArray(),
        ]);

        return $response;
    }

    /**
     * Retrieve the details of the given order
     *
     * @param Request $request
     * @param Response $response
     * @return ResponseInterface
     */
    public function show(Request $request, Response $response): ResponseInterface
    {
        $model = Orders::with(['products', 'products.product'])
            ->find($request->getParam('id'));

        $response->setBody($model->toArray());

        return $response;
    }

    /**
     * Deletes an item from an order
     *
     * @param Request $request
     * @param Response $response
     * @return ResponseInterface
     */
    public function delete(Request $request, Response $response): ResponseInterface
    {
        $order = Orders::find($request->getParam('id'));
        if (!$order) {
            $response->setCode(404);
            $response->setBody(['status' => 'Not Found']);

            return $response;
        }

        $item = OrdersItems::find($request->getParam('item_id'));
        if (!$item) {
            $response->setCode(404);
            $response->setBody(['status' => 'Not Found']);

            return $response;
        }

        $item->delete();
        $response->setBody(['status' => 'success']);

        return $response;
    }

    /**
     * Stripe payment
     *
     * @param Request $request
     * @param Response $response
     * @return ResponseInterface
     * @throws ApiErrorException
     */
    public function payment(Request $request, Response $response): ResponseInterface
    {
        $stripe = new \Stripe\StripeClient(
            'sk_test_51KWiL3CqaYeH0IktdEhnmn2rFjKVRGIFtG7v24MdUL4aNZhJwyz8C9oW6mB7Cib4Eriu3EK11WgdmGdLOV61Xe3F00gNf6NppC'
        );
        $paymentMethod = $stripe->paymentMethods->create([
            'type' => 'card',
            'card' => [
                'number' => '4242424242424242',
                'exp_month' => 8,
                'exp_year' => 2023,
                'cvc' => '314',
            ],
        ]);

        $response->setBody($paymentMethod->toArray());

        return $response;
    }
}
