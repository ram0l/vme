<?php

namespace App\Controllers;

use App\Models\Orders;

class ErrorController extends BaseController
{
    public function json(mixed $data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
