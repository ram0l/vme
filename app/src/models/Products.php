<?php

namespace App\Models;

class Products extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'products';
    protected $fillable = [
        'name',
        'barcode',
        'brand',
        'price',
        'image_url',
        'date_added',
    ];
}
