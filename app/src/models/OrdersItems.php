<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $order_id
 * @property int $quantity
 * @property int $product_id
 */
class OrdersItems extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'orders_items';
    public $timestamps = false;
    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
    ];

    /**
     * @return BelongsTo
     */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }
}
