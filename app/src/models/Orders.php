<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property int $user_id
 * @property int $sum
 * @property string $status
 */
class Orders extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'orders';
    protected $fillable = [
        'user_id',
        'status',
        'sum'
    ];

    /**
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(OrdersItems::class, 'order_id', 'id');
    }
}
