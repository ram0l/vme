<?php

namespace App\Models;

use Vme\Request\Request;

abstract class BaseModel extends \Illuminate\Database\Eloquent\Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
